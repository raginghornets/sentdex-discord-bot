#!/usr/bin/env python3
import discord
import time
import asyncio

client = discord.Client()
token = open("token.txt", "r").read()

def community_report(guild):
    online = 0
    idle = 0
    offline = 0

    for m in guild.members:
        if str(m.status) == "online":
            online += 1
        elif str(m.status) == "offline":
            offline += 1
        else:
            idle += 1
    
    return online, idle, offline

async def user_metrics_background_task(guild):
    await client.wait_until_ready()

    while not client.is_closed():
        try:
            online, idle, offline = community_report(guild)
            with open("usermetrics.csv","a") as f:
                f.write(f"{int(time.time())},{online},{idle},{offline}\n")
            await asyncio.sleep(5)

        except Exception as e:
            print(str(e))
            await asyncio.sleep(5)

@client.event # event decorator/wrapper
async def on_ready():
    print(f"We have logged in as {client.user}")

@client.event
async def on_message(message):
    guild_id = open("guild.txt","r").read()
    guild = client.get_guild(int(guild_id))

    print(f"{message.channel}: {message.author}: {message.author.name}: {message.content}")

    if "tutorialbot.member_count()" == message.content:
        await message.channel.send(f"{guild.member_count} members in {guild}")
    elif "tutorialbot.logout()" == message.content:
        await message.channel.send("Goodbye!")
        await client.close()
    elif "tutorialbot.community_report()" == message.content:
        online, idle, offline = community_report(guild)
        
        await message.channel.send(f"{online} members online.\n{idle} members idle.\n{offline} members offline.")
    elif "hi" in message.content.lower():
        await message.channel.send(f"Hello, {message.author.name}.")

# client.loop.create_task(user_metrics_background_task())
client.run(token)
